package fr.esimed.tp.test

import fr.esimed.tp.DataLoader
import fr.esimed.tp.levenshteinDistance
import fr.esimed.tp.percentMatch
import fr.esimed.tp.top10
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.function.Executable

class TextToolsTests {

    @Test
    fun levenshteinDistanceTest(){
        Assertions.assertAll(
            Executable { Assertions.assertEquals(6, levenshteinDistance("Arnaud", "Julien")) },
            Executable { Assertions.assertEquals(1, levenshteinDistance("Julie", "Julien")) }
        )
    }

    @Test
    fun percentMatchtest(){
        Assertions.assertAll(
            Executable { Assertions.assertEquals(50, percentMatch("Arnaud", "Julien")) },
            Executable { Assertions.assertEquals(90, percentMatch("Julie", "Julien")) }
        )
    }

    @Test
    fun top10(){
        Assertions.assertAll(
            Executable { Assertions.assertEquals(listOf("OLIVIER","OLIVIERO","OLLIVIER","LIVIER","OLIVER","OLIVIEN","OLIWIER","OLIVERA","OLIVINE","OLIVIO"), top10("OLIVIER", DataLoader.loadUniqueFirstnames("src/resoucres/nat2017.txt"))) }
        )
    }
}
