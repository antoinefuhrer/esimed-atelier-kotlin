package fr.esimed.tp

import java.io.FileReader

object DataLoader {

    // SIMPLIFIER & OPTIMISER

    fun loadUniqueFirstnames(path:String): List<String>{
        var result: MutableSet<String> = mutableSetOf()
        var reader = FileReader(path)
        reader.forEachLine {result.add(it.split("\t")[1]) }
        return result.toList()
    }

    // SIMPLIFIER & OPTIMISER

    fun load(path:String): Collection<Firstname>{
        var result: MutableList<Firstname> = mutableListOf()
        var reader = FileReader(path)
        var valid = false
        var list:List<String>
        reader.forEachLine {
            if (!valid) {
                valid = true
            } else {
                list = it.split("\t")
                if (list[2] != "XXXX")
                    result.add(Firstname(list[0].toInt(), list[1], list[2], list[3].toInt()))
            }
        }
        return result
    }
}