package fr.esimed.tp

fun main(){
    println("TP 1 : base du langage et scripts")
    //println("Distance de Levenshtein pour \"Arnaud\" et \"Julien\" = ${levenshteinDistance("Arnaud","Julien")}")
    //println("Distance de Levenshtein pour \"Julie\" et \"Julien\" = ${levenshteinDistance("Julie","Julien")}")
    //println("Pourcentages de proximité entre \"Arnaud\" et \"Julien\" = ${percentMatch("Arnaud", "Julien")}")
    //println("Pourcentages de proximité entre \"Julie\" et \"Julien\" = ${percentMatch("Julie", "Julien")}")

    // QUESTION 18
    /*println()
    println("Question 18 - affichage des 10 prénoms les plus proches de \"OLIVIER\" :")
    var firstNames1: List<String> = DataLoader.loadUniqueFirstnames("src/resoucres/nat2017.txt")
    top10("OLIVIER", firstNames1).forEach{println(it)}*/

    //QUESTION 22
    //var firstNames2: List<Firstname> = DataLoader.load("src/resoucres/nat2017.txt")

    // QUESTION 23
    /*println()
    println("Question 23 - affichage des données associées au prénom \"OLIVIER\" :")
    groupByFirstname("OLIVIER", firstNames2).forEach{println(it)}*/

    //QUESTION 24
    /*println()
    println("Question 24 - affichage des statistiques des 10 prénoms les plus proches de \"OLIVIER\" :")
    var list1: List<String> = DataLoader.loadUniqueFirstnames("src/resoucres/nat2017.txt")
    var list2: List<Firstname> = DataLoader.load("src/resoucres/nat2017.txt")
    top10("OLIVIER", list1).forEach{
        println("$it :")
        groupByFirstname(it, list2).forEach{println("       ${it.year} = ${it.count}")}
    }*/

    // QUESTION 25
    println("Question 25 - affichage des 10 prénoms les plus proches de \"OLIVIER\" avec  les années où ces prénoms ont été les plus données. :")
    var list1: List<String> = DataLoader.loadUniqueFirstnames("src/resoucres/nat2017.txt")
    var list2: Collection<Firstname> = DataLoader.load("src/resoucres/nat2017.txt")
    var firstname:Firstname
    top10("OLIVIER", list1).forEach{
        //firstname = maxCount(groupByFirstname(it, list2.toList()))
        firstname = groupByFirstname(it, list2.toList()).maxBy { it ->  it.count }!!
        println("$it: ${firstname.year}, ${firstname.count}")
    }
}