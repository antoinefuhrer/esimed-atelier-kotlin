package fr.esimed.tp

// UTLISER MAXBY ET GROUPBY

fun groupByFirstname(firstname:String, listFirstName:List<Firstname>): List<Firstname>{
    var percentMatchResults: MutableList<Firstname> = mutableListOf()
    listFirstName.forEach {if (it.firstname == firstname) percentMatchResults.add(it)}
    return percentMatchResults
}

fun top10(prenom:String, listPrenoms:List<String>): List<String>{
    var percentMatchResults: MutableMap<String, Int> = mutableMapOf()
    listPrenoms.forEach{percentMatchResults.put(it, percentMatch(it, prenom))}
    return percentMatchResults.toList().sortedByDescending { (_, value) -> value}.take(10).toMap().keys.toList()
}

fun percentMatch(str1 : String, str2 : String): Int{
    return (100f - (levenshteinDistance(str1, str2) * 100f) / (str1.length + str2.length)).toInt()
}

fun levenshteinDistance(lhs : String, rhs : String) : Int {
    //https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Kotlin
    val lhsLength = lhs.length
    val rhsLength = rhs.length

    var cost = IntArray(lhsLength + 1) { it }
    var newCost = IntArray(lhsLength + 1) { 0 }

    for (i in 1..rhsLength) {
        newCost[0] = i

        for (j in 1..lhsLength) {
            val editCost= if(lhs[j - 1] == rhs[i - 1]) 0 else 1
            val costReplace = cost[j - 1] + editCost
            val costInsert = cost[j] + 1
            val costDelete = newCost[j - 1] + 1

            newCost[j] = minOf(costInsert, costDelete, costReplace)
        }

        val swap = cost
        cost = newCost
        newCost = swap
    }

    return cost[lhsLength]
}